import numpy as np
import re
import pandas as pd

def loadFile(filename):
    csv_file_object = pd.read_csv(filename) # Read CSV file
    header = list(csv_file_object)
    X = np.array(csv_file_object) # Then convert from a list to an array.
    return [header, X]

def preprocess_test(filename):
    header, X = loadFile(filename)
    header = [header[0]] + ["Survived"] + header[1:]
    N = X.shape
    X2 = np.zeros((N[0],1))
    X = np.insert(X, [1], X2, axis=1)
    return preprocess(header, X)

def preprocess_train(filename):
    header, X = loadFile(filename)
    return preprocess(header, X)

def preprocess(header, X):
    
    y = X[:,1].astype(int) # Save labels to y 

    X = np.delete(X,1,1) # Remove survival column from matrix X
    header.remove("Survived")
    X = np.delete(X,0,1) # Remove PassengerId column from matrix X
    header.remove("PassengerId")
    

    # Get mean value of age
    ageMean = 0
    number = 0

    embarked = {}

    titles = {
        'Mr.': 1, 
        'Mrs.': 2, 
        'Miss.': 3, 
        'Master.': 4, 
        'Don.': 5, 
        'Rev.': 5,
        'Dr.': 4, 
        'Mme.': 2, 
        'Ms.': 6, 
        'Major.': 7, 
        'Lady.': 6, 
        'Sir.': 1, 
        'Mlle.': 3, 
        'Col.': 7, 
        'Capt.': 7, 
        'Countess.': 5, 
        'Jonkheer.': 5,
        'Dona.': 5,
    }

    for row in X:
        # embarked
        dock = row[9]
        if isinstance(dock, basestring):
            if dock in embarked:
                embarked[dock] += 1
            else:
                embarked[dock] = 0

        # age
        try:
            toAdd = int(row[3])
            if toAdd > 0:
                ageMean += toAdd
                number += 1
        except ValueError:
            continue
    ageMean /= number

    cabin_list = ['A', 'B', 'C', 'D', 'E', 'F', 'T', 'G']

    # Loop over all elements to modify
    for i in range(X.shape[0]):

        # Titles 
        
        match = re.search(r"([A-Z][a-z]{1,8}\.)", X[i][1])
        if match:
            X[i][1] = titles[match.group(1)]
        else:
            X[i][1] = 0
            print 'title not valide'

        # Male / Female
        if X[i][2] == "male":
            X[i][2] = 0
        elif X[i][2] == "female":
            X[i][2] = 1
        else:
            print "gender not good"

        # Age
        try:
            X[i][3] = int(X[i][3])
            if X[i][3] <= 0:
                raise ValueError("Age not valid")
        except ValueError:
            X[i][3] = ageMean
        
        # NbFamily
        try:
            X[i][4] = int(X[i][4])
            X[i][5] = int(X[i][5])
            if X[i][4] < 0 or X[i][5] < 0:
                raise ValueError("Family not valid")
            X[i][4] += X[i][5]
        except ValueError:
            X[i][4] = 0

        # Deck
        modified = False
        if isinstance(X[i][8], basestring):
            for cabin in cabin_list:
                if cabin in X[i][8]:
                    X[i][8] = cabin_list.index(cabin)+1
                    modified = True
                    break
        if not modified:
            X[i][8] = 0
        


        # Embarked
        while True:
            if not isinstance(X[i][9], float):
                if 'C' in X[i][9]:
                    X[i][9] = 0
                    break
                elif 'Q' in X[i][9]:
                    X[i][9] = 1
                    break
                elif 'S' in X[i][9]:
                    X[i][9] = 2
                    break
                else:
                    X[i][9] = max(embarked, key=embarked.get)
            else:
                X[i][9] = max(embarked, key=embarked.get)

    header.remove("Parch")
    X = np.delete(X, 5, 1)

    header.remove("Ticket")
    X = np.delete(X, 5, 1)

    header.remove("Fare")
    X = np.delete(X, 5, 1)

    header[1] = "Title"
    header[4] = "NbFamily"
    header[5] = "Deck"
    
  
    return [y, X]