import pandas as pd
import numpy as np
from preprocess import loadFile

def predicated_class_csv(Survived, fileName):
    '''
    fileName : csv of testSet
    Survived : Prediction made on test set
    
    Write it in the csv file : predicated_class.csv
    '''    
    try:
        PassengerId = loadFile(fileName)[1][:, 0]
        X = np.column_stack(([PassengerId, Survived]))
        names = ["PassengerId", "Survived"]
        df = pd.DataFrame(X, columns=names)
        df.to_csv('predicated_class.csv', header=True, index=False)
    except:
        print("Error in predicated_class_csv function.")

