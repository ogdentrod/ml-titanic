from sklearn.naive_bayes import GaussianNB

def classify(trainSet, trainLabels, testSet):
    classifier = GaussianNB().fit(trainSet, trainLabels)
    return classifier.predict(testSet)
