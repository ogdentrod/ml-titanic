

from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.svm import SVC

from sklearn.model_selection import GridSearchCV

import numpy as np

"""
This class initializes multiple classifiers and defines the set of parameters we want to try for these classifiers.
It used the GridSearchCV (Which stands for grid search cross validation) which takes a "grid" of parameters and does 
an exhaustive search. It does this by training the model with cross validation and compares the cross validated score to
the rest of the parameters. It then keeps the best classifier. 
"""

class ModelComparator:
    def __init__(self, CVFolds):
        self.classifiers = {
            "logistic" : LogisticRegression(),
            "gaussianNaiveBayes" : GaussianNB(),
            "kNN" : KNeighborsClassifier(),
            "randomForest" : RandomForestClassifier(),
            "adaBoost" : AdaBoostClassifier(),
            "svm" : SVC(gamma="scale")
        }

        self.param_grid = {
            #Test different penalties with different weights 
            "logistic" : {'penalty' : ['l1','l2'], 'C' : np.logspace(-5,5,20)},

            #Gaussian Naive Bayes doesn't have any parameters to tune
            "gaussianNaiveBayes" : {},

            #Test for different number of neighbors (around sqrt(Number of rows)) and for different distances (l1 and l2)
            "kNN" : {"p" : [1,2], "n_neighbors":[5,10,20,25,30,35,40]},

            #Ensemble methods : we vary the number of estimators
            "randomForest" : {"n_estimators" : [5,10,20,30]},
            "adaBoost" : {"n_estimators" : [5,10,20,30]},

            #We change the type of kernel and the regulirization strenghts
            "svm" : [
                {'C' : [0.01,0.1, 1.0, 10.0, 100.0]},
                {'kernel': ['poly'], 'degree':[1,2,3,4,5]},
            ]
        }
        self.gridSearchClassifiers = {}

        for key in self.classifiers:
            self.gridSearchClassifiers[key] = GridSearchCV(
                                                self.classifiers[key],
                                                self.param_grid[key], 
                                                iid=False, #This assures we have the mean of scores for all folds
                                                scoring="accuracy", 
                                                cv=CVFolds,
                                                n_jobs=1)
    
    def fit(self,X,y):
        print "Training on grid of parameters"
        for key in self.gridSearchClassifiers:
            print "    - "+key+""
            self.gridSearchClassifiers[key].fit(X,y)

    def getBestClassifier(self):
        best = ""
        bestScore = 0.
        for key in self.classifiers:
            if bestScore < self.gridSearchClassifiers[key].best_score_:
                best = key
                bestScore = self.gridSearchClassifiers[key].best_score_
        
        return self.gridSearchClassifiers[best]