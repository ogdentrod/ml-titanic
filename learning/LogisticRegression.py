from sklearn.linear_model import LogisticRegression

def classify(trainSet, trainLabels, testSet):
    classifier = LogisticRegression().fit(trainSet, trainLabels)
    return classifier.predict(testSet)
