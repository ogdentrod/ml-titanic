from sklearn.model_selection import KFold
from numpy import *

def classify(trainSet, trainLabels, testSet):
	
	predictedLabels = zeros(testSet.shape[0])
	
	for i in range(testSet.shape[0]):
		if testSet[i,2] == 1:
			predictedLabels[i] = 1

	return predictedLabels

def crossValidationScore(X, y, cv=5):
	kf = KFold(n_splits=cv)

	totalInstances = 0 # Variable that will store the total intances that will be tested  
	totalCorrect = 0 # Variable that will store the correctly predicted intances  

	for trainIndex, testIndex in kf.split(X):
		trainSet = X[trainIndex]
		testSet = X[testIndex]
		trainLabels = y[trainIndex]
		testLabels = y[testIndex]
		
		predictedLabels = classify(trainSet, trainLabels, testSet)

		correct = 0	
		for i in range(testSet.shape[0]):
			if predictedLabels[i] == testLabels[i]:
				correct += 1

		totalCorrect += correct
		totalInstances += testLabels.size
	return totalCorrect/float(totalInstances)
	