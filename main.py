from numpy import *
from learning import baseline, training
from LDA import LDA
from preprocess import preprocess_test, preprocess_train
from predicated_class_csv import predicated_class_csv

def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

CVFolds = 10
nFeatures = 6 #optimal found

y, X = preprocess_train("Data/train.csv")
X = X.astype(float32)

W = LDA(X, y, nFeatures) #Projecting space
X = dot(X, W)

modelComparator = training.ModelComparator(CVFolds)

modelComparator.fit(X,y)

bestClf = modelComparator.getBestClassifier()

print "Baseline (Predicting females survive) score : "+str(baseline.crossValidationScore(X,y,CVFolds))

print "Best estimators (cross validated score): "
for key in modelComparator.classifiers:
    print "    " +key + ": \n\tbest score : " + str(modelComparator.gridSearchClassifiers[key].best_score_) + "  with parameters : " + str(modelComparator.gridSearchClassifiers[key].best_params_)

#Apply best classifier on test set
Y_predictedTestSet, X_testSet = preprocess_test("Data/test.csv")
X_testSet = X_testSet.astype(float32)

X_testSet = dot(X_testSet, W)

#bestClassifier = modelComparator.getBestClassifier()
Y_predictedTestSet = modelComparator.gridSearchClassifiers["logistic"].predict(X_testSet)

predicated_class_csv(Y_predictedTestSet, "Data/test.csv")

