import numpy as np
import scipy as sp
import scipy.linalg as linalg
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

def skLDA(X, Y):
    clf = LinearDiscriminantAnalysis()
    return clf.fit_transform(X, Y)

def LDA(X, Y, k):
    """
    Train a LDA classifier from the training set
    X: training data
    Y: class labels of training data
    k: number of features wanted

    return W: optimal new space.
    """
    #Fixed values
    classLabels = np.array([0,1])
    classNum = 2

    #Get values from inputs
    datanum, dim = X.shape # dimensions of the dataset (rows, columns)
    totalMean = np.mean(X,0) # total mean of the data (array of means of each feature)

    #Create two arrays, the index of Y where label = 0 and the index where label = 1
    partition = [np.where(Y==label)[0] for label in classLabels]

    #Get two arrays : one for each label
    #Each column of an array is the mean of datas for this label.
    classMean, class_size = [], []
    for idx in partition:
        classMean.append(np.mean(X[idx],0))  #[array([2.5318761384335153, nan, 0.3296903460837887], dtype=object), array([1.9502923976608186, nan, 0.4649122807017544], dtype=object)]
        class_size.append(len(idx)) #[549, 342]

    # Compute the within-class scatter matrix
    Sw = np.zeros((dim, dim))
    for n in range(classNum):
        Sw += np.cov(X[partition[n]], rowvar=0) * class_size[n]
    #Sw : [[552.1338699   18.52544577   0.        ]
    #[ 18.52544577 143.65318835   0.        ]
    #[  0.           0.           0.        ]]

    # Compute the between-class scatter matrix
    Sb = np.zeros((dim,dim))
    for n in range(classNum):
        mu = classMean[n].reshape(dim, 1)
        Sb += class_size[n] * np.dot((mu - totalMean), np.transpose((mu - totalMean)))
    #Sb : [[ 789108.41256927  837040.67312711   55161.74410774]
    #[ 837040.67312711  895988.46345199  -51752.03703704]
    #[  55161.74410774  -51752.03703704 1677883.5342312 ]]

    #Compute sb/sw
    try:
        S = np.dot(linalg.inv(Sw), Sb) #sb/sw
        eigval, eigvec = linalg.eig(S)
    except:
        print("Singular matrix")
        eigval, eigvec = linalg.eig(Sb, Sw+Sb)
    
    idx = eigval.argsort()[::-1] # Sort desc eigenvalues
    eigvec = eigvec[:,idx] # Sort eigenvectors according to eigenvalues
    W = np.real(eigvec[:,:k]) # eigenvectors correspond to k largest eigenvalues

    return W